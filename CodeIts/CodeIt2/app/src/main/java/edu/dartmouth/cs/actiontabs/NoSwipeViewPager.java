package edu.dartmouth.cs.actiontabs;

import android.support.v4.view.ViewPager;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by Tang on 4/8/2018.
 */

public class NoSwipeViewPager extends ViewPager {
    public NoSwipeViewPager(Context ctxt, AttributeSet at) {
        super(ctxt, at);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return false;
    }
}
