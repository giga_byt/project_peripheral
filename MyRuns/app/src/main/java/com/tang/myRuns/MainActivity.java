package com.tang.myRuns;

import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    //UI Elements
    private Toolbar actionBar;
    private NoSwipeViewPager pager;
    private BottomNavigationView bottomNav;
    private ArrayList<Fragment> fragments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Instantiate and set up views
        setContentView(R.layout.activity_main);
        actionBar = findViewById(R.id.main_activity_toolbar);
        setSupportActionBar(actionBar);
        pager= findViewById(R.id.main_viewpager);
        bottomNav = findViewById(R.id.bottom_navigation_main);

        //set up viewPager and Fragments
        fragments = new ArrayList<>();
        fragments.add(new StartFragment());
        fragments.add(new HistoryFragment());
        fragments.add(new LeaderboardFragment());
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), fragments);
        pager.setAdapter(viewPagerAdapter);

        //attach the listeners to the bottom navigation
        attachNavListeners(bottomNav);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_action_bar, menu);
        return true;
    }

    /* Navigation to Profile and Settings from the Options Menu */
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();

        if(id == R.id.main_toolbar_profile){
            Intent registration = new Intent(getApplicationContext(), RegistrationActivity.class);
            registration.putExtra("SENDING_ACTIVITY_ID", "MainActivity");
            getApplicationContext().startActivity(registration);
            return true;
        } else if (id == R.id.main_toolbar_settings){
            Intent settings = new Intent(getApplicationContext(), SettingsActivity.class);
            settings.putExtra("SENDING_ACTIVITY_ID", "MainActivity");
            getApplicationContext().startActivity(settings);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /* Creates and attaches listeners to the Bottom Navigation View to tie the
     * Viewpager together with the Bottom Navigation.
     */
    public void attachNavListeners(BottomNavigationView nav){
        nav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.main_nav_history:
                        pager.setCurrentItem(1);
                        return true;
                    case R.id.main_nav_start:
                        pager.setCurrentItem(0);
                        return true;
                    case R.id.main_nav_leaderboard:
                        pager.setCurrentItem(2);
                        return true;
                }
                return false;
            }
        });
        nav.setOnNavigationItemReselectedListener(new BottomNavigationView.OnNavigationItemReselectedListener() {
            @Override
            public void onNavigationItemReselected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.main_nav_history:
                        pager.setCurrentItem(1);
                    case R.id.main_nav_start:
                        pager.setCurrentItem(0);
                    case R.id.main_nav_leaderboard:
                        pager.setCurrentItem(2);
                }
            }
        });
    }
}
