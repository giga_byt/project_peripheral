package com.tang.myRuns;

import android.support.v4.content.AsyncTaskLoader;
import android.content.Context;
import android.support.annotation.Nullable;

import java.util.ArrayList;

/**
 * Created by Tang on 4/24/2018.
 */

public class SingleExerciseEntryLoader extends AsyncTaskLoader<ExerciseEntry> {
    private ExerciseEntryDbHelper db;
    private long idNum;

    SingleExerciseEntryLoader(Context context, long idNum){
        super(context);
        this.idNum = idNum;
        db = new ExerciseEntryDbHelper(context);
    }

    @Override
    @Nullable
    public ExerciseEntry loadInBackground(){
        db.openDb();
        ExerciseEntry entry = db.fetchEntryByIndex(this.idNum);
        db.closeDb();
        return entry;
    }
}
