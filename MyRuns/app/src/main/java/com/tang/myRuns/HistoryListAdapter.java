package com.tang.myRuns;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Tang on 4/23/2018.
 */

public class HistoryListAdapter extends BaseAdapter {
    Context context;
    ArrayList<ExerciseEntry> data;
    private static LayoutInflater inflater = null;

    public HistoryListAdapter(Context context){
        this.context = context;
        this.data = new ArrayList<>();
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount(){
        return data.size();
    }

    public ExerciseEntry getItem(int position){
        return data.get(position);
    }

    public long getItemId(int position){
        return position;
    }

    public void setData(ArrayList<ExerciseEntry> data){
        this.data = data;
    }
    public View getView(int position, View convertView, ViewGroup parent){
        ExerciseEntry entry = data.get(position);

        View vi = convertView;
        if(vi == null){
            vi = inflater.inflate(R.layout.history_row_item, null);
        }
        TextView title = vi.findViewById(R.id.history_activity_title);
        TextView time = vi.findViewById(R.id.history_time_title);
        TextView comment = vi.findViewById(R.id.history_activity_comment);

        int activityType = entry.getmActivityType();
        int inputType = entry.getmInputType();

        String titleText = StartFragment.ID_TO_INPUT_MAP.get(inputType) + ":" +
                StartFragment.ID_TO_ACTIVITY_MAP.get(activityType);

        String timeText = entry.getmDateTime();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        boolean isMetric = preferences.getString("unit_switch", "Metric (Kilometers)").
                equals("Metric (Kilometers)");
        String commentText = entry.getmDistance() + " kms, " + entry.getmDuration() + " mins ";
        if(!isMetric) {
            commentText = entry.getmDistance() * 0.58 + " miles, " + entry.getmDuration() + " mins ";
        }
        title.setText(titleText);
        time.setText(timeText);
        comment.setText(commentText);
        return vi;
    }

    public ArrayList<ExerciseEntry> getData() {
        return data;
    }
}
