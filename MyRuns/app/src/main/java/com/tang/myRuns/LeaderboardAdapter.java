package com.tang.myRuns;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Tang on 5/14/2018.
 */

public class LeaderboardAdapter extends BaseAdapter {
    Context context;
    ArrayList<JSONObject> data;
    private static LayoutInflater inflater = null;

    public LeaderboardAdapter(Context context){
        this.context = context;
        this.data = new ArrayList<>();
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount(){
        return data.size();
    }

    public JSONObject getItem(int position){
        return data.get(position);
    }

    public long getItemId(int position){
        return position;
    }

    public void setData(ArrayList<JSONObject> data){
        this.data = data;
    }
    public View getView(int key, View convertView, ViewGroup parent){
        JSONObject jsonObject = data.get(key);

        View vi = convertView;
        if(vi == null){
            vi = inflater.inflate(R.layout.leaderboard_row_item, null);
        }
        TextView title = vi.findViewById(R.id.leaderboard_activity_title);
        TextView time = vi.findViewById(R.id.leaderboard_time_title);
        TextView comment = vi.findViewById(R.id.leaderboard_activity_comment);
        TextView emailField = vi.findViewById(R.id.leaderboard_activity_email);
        String distance = "";
        String input_type = "";
        String duration = "";
        String activity_type = "";
        String email = "";
        String activity_date = "";
        try {
            distance = jsonObject.getString("distance");
            input_type = jsonObject.getString("input_type");
            duration = jsonObject.getString("duration");
            activity_type = jsonObject.getString("activity_type");
            email = jsonObject.getString("email");
            activity_date = jsonObject.getString("activity_date");
        } catch (JSONException e){
            e.printStackTrace();
        }

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        boolean isMetric = preferences.getString("unit_switch", "Metric (Kilometers)").
                equals("Metric (Kilometers)");
        String commentText = distance + " kms, " + duration + " mins ";
        if(!isMetric) {
            commentText = Double.parseDouble(distance) * 0.58 + " miles, " + duration + " mins ";
        }
        String titleText = input_type + ":" + activity_type;
        title.setText(titleText);
        time.setText(activity_date);
        comment.setText(commentText);
        emailField.setText(email);
        return vi;
    }

    public ArrayList<JSONObject> getData() {
        return data;
    }
}
