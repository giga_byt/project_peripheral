package com.tang.myRuns;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ManualActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<ExerciseEntry>{

    private final int LOADER_ID = 200;

    private String activity;
    private String date;
    private String time;
    private int duration;
    private double distance;
    private int calorie;
    private int heartbeat;
    private String comment;

    Calendar calendar = Calendar.getInstance();
    private List<Map<String, String>> fields;

    SimpleAdapter adapter;
    private Button saveButton;
    private LoaderManager mLoader;

    private boolean isRecord = false;
    private boolean isMetric = true;
    private long recordId;
    private ExerciseEntry dataEntry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        isMetric = preferences.getString("unit_switch", "Metric (Kilometers)").
                equals("Metric (Kilometers)");
        //Initialize Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        if (getIntent().getStringExtra("SENDING_ACTIVITY_ID").equals("HistoryFragment")){
            isRecord = true;
            recordId = getIntent().getLongExtra("ID", -1);
            mLoader = getSupportLoaderManager();
            mLoader.initLoader(LOADER_ID, null, this).forceLoad();
        }
        //Initialize List
        initializeList();
        if(!isRecord) {
            saveButton = findViewById(R.id.save_button);
            saveButton.setText(R.string.save_button);
            saveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    saveFieldsToDatabase();
                }
            });
        } else {
            saveButton = findViewById(R.id.save_button);
            saveButton.setText(R.string.button_delete);
            saveButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    DeleteDatabaseEntryTask run = new DeleteDatabaseEntryTask(getApplicationContext());
                    run.execute(recordId);
                    finish();
                }
            });
        }
    }

    protected void onResume(){
        super.onResume();
    }

    protected void onPause(){
        super.onPause();
    }

    private void saveFieldsToDatabase(){
        ExerciseEntry entry = new ExerciseEntry();
        entry.setmInputType(StartFragment.INPUT_TO_ID_MAP.get("Manual"));
        entry.setmActivityType(StartFragment.ACTIVITY_TO_ID_MAP.get(activity));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String dt = date + " " + time;
        Calendar dateTime = Calendar.getInstance();
        try {
            dateTime.setTime(sdf.parse(dt));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        entry.setmDateTime(dateTime);
        entry.setmDuration(duration);
        entry.setmDistance(distance);
        entry.setmComment(comment);
        entry.setmCalorie(calorie);
        entry.setmHeartRate(heartbeat);

        InsertDatabaseEntryTask run = new InsertDatabaseEntryTask(getApplicationContext());
        run.execute(entry);

        Toast toast = Toast.makeText(this, "Saved Activity Details!", Toast.LENGTH_SHORT);
        toast.show();
        finish();
    }

    private List<Map<String, String>> generateList(String[] titles, String[] subtitles){
        List<Map<String, String>> fields = new ArrayList<>();
        for(int i = 0; i < titles.length; i++){
            Map<String, String> datum = new HashMap<>(2);
            datum.put("First Line", titles[i]);
            datum.put("Second Line", subtitles[i]);
            fields.add(datum);
        }
        return fields;
    }

    private String[] generateDescriptions(){
        //fetch information;

        activity = getIntent().getStringExtra("ACTIVITY_TYPE");
        Date d = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        date = df.format(d);
        time = Calendar.getInstance().get(Calendar.HOUR_OF_DAY) + ":" +
                Calendar.getInstance().get(Calendar.MINUTE);
        duration = 0;
        distance = 0;
        calorie = 0;
        heartbeat = 0;
        comment = "";

        // if this is from record, update information from database
        if(isRecord && dataEntry != null){
            duration = dataEntry.getmDuration();
            distance = dataEntry.getmDistance();
            calorie = dataEntry.getmCalorie();
            heartbeat = dataEntry.getmHeartRate();
            date = df.format(dataEntry.getmDateTimeCal().getTime());
            time = dataEntry.getmDateTimeCal().get(Calendar.HOUR_OF_DAY) + ":" +
                    dataEntry.getmDateTimeCal().get(Calendar.MINUTE);
            activity = StartFragment.ID_TO_ACTIVITY_MAP.get(dataEntry.getmActivityType());
            comment = dataEntry.getmComment();
        }

        //if in Imperial, convert miles to kilometers and generate string accordingly
        String distanceText = distance + " km";
        if(!isMetric){
            double miles = distance * 0.58;
            distanceText = miles + " miles";
        }
        return new String[]{
                activity,
                date,
                time,
                duration + " mins",
                distanceText,
                calorie + " cals",
                heartbeat + " bpm",
                comment
        };
    }

    private void initializeList() {
        String[] titles = this.getResources().getStringArray(R.array.manual_input_titles);
        String[] subtitles = generateDescriptions();
        fields = generateList(titles, subtitles);

        adapter = new SimpleAdapter(this, fields,
                android.R.layout.simple_list_item_2, new String[]{"First Line", "Second Line"},
                new int[]{android.R.id.text1, android.R.id.text2});
        ListView mainList = findViewById(R.id.manual_entry_list_view);
        mainList.setAdapter(adapter);
        if (!isRecord) {
            mainList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    switch (position) {
                        //Activity
                        case 0:
                            return;

                        //Date
                        case 1:
                            new DatePickerDialog(ManualActivity.this, datePicker, calendar.get(Calendar.YEAR),
                                    calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
                            return;

                        //Time
                        case 2:
                            TimePickerDialog timePicker = new TimePickerDialog(ManualActivity.this,
                                    new TimePickerDialog.OnTimeSetListener() {
                                        @Override
                                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                            time = hourOfDay + ":" + minute;
                                            fields.get(2).put("Second Line", time);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE),
                                    true);
                            timePicker.setTitle("Select Time");
                            timePicker.show();
                            return;

                        //Duration
                        case 3:
                            editListField(3, "min");
                            return;

                        //Distance
                        case 4:
                            if(isMetric) {
                                editListField(4, "km");
                            } else {
                                editListField(4, "ml");
                            }
                            return;

                        //Calorie
                        case 5:
                            editListField(5, "cal");
                            return;

                        //Heartbeat
                        case 6:
                            editListField(6, "bpm");
                            return;

                        //Comment
                        case 7:
                            editListField(7, " ");
                            return;

                    }
                }
            });
        }
    }

    DatePickerDialog.OnDateSetListener datePicker = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateDateView();
        }

    };
    private void updateDateView(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd", Locale.US);
        date = sdf.format(calendar.getTime());
        fields.get(1).put("Second Line", date);
        adapter.notifyDataSetChanged();
    }

    private void editListField(final int field, final String suffix){
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        final EditText input = new EditText(this);
        if(field != 7){
            //Isn't comments, therefore only numerals
            input.setInputType(InputType.TYPE_CLASS_NUMBER);
        }
        alert.setView(input);

        alert.setPositiveButton("EDIT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String value = input.getText().toString();
                if(!value.equals("")) {
                    updateVar(field, value);
                }
                fields.get(field).put("Second Line", value + " " + suffix);
                adapter.notifyDataSetChanged();
            }
        });
        alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
        alert.show();
    }

    private void updateVar(int field, String value){
        switch(field){
            //Activity
            case 0:
                return;

            //Date
            case 1:
                return;

            //Time
            case 2:
                return;

            //Duration
            case 3:
                duration = Integer.parseInt(value);
                return;

            //Distance
            case 4:
                if(isMetric) {
                    distance = Double.parseDouble(value);
                } else {
                    distance = Double.parseDouble(value) * 1.7;
                }
                return;

            //Calorie
            case 5:
                calorie = Integer.parseInt(value);
                return;

            //Heartbeat
            case 6:
                heartbeat = Integer.parseInt(value);
                return;

            //Comment
            case 7:
                comment = value;
                return;
        }
    }

    @NonNull
    @Override
    public Loader<ExerciseEntry> onCreateLoader(int id, @Nullable Bundle args) {
        if(id == LOADER_ID) {
            return new SingleExerciseEntryLoader(getApplicationContext(), recordId);
        }
        return null;
    }

    @Override
    public void onLoadFinished(@NonNull Loader<ExerciseEntry> loader, ExerciseEntry data) {
        if(loader.getId() == LOADER_ID) {
            dataEntry = data;
            initializeList();
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<ExerciseEntry> loader) {
    }
}
