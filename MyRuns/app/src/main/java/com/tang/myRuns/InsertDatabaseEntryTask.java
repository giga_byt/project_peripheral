package com.tang.myRuns;

import android.content.Context;
import android.os.AsyncTask;

/**
 * Created by Tang on 4/24/2018.
 */

public class InsertDatabaseEntryTask extends AsyncTask<ExerciseEntry, Void, Void> {

    private ExerciseEntryDbHelper datasource;

    InsertDatabaseEntryTask(Context context){
        super();
        datasource = new ExerciseEntryDbHelper(context);
    }
    @Override
    protected void onPreExecute(){
        datasource.openDb();
    }
    protected Void doInBackground(ExerciseEntry... entries){
        //put the entry into the db

        for(int i = 0; i < entries.length; i++){
            datasource.insertEntry(entries[i]);
        }
        return null;
    }
    protected void onProgressUpdate(Void... params){
        super.onProgressUpdate();
    }

    protected void onPostExecute(Void result){
        datasource.closeDb();
        super.onPostExecute(result);
    }
}
