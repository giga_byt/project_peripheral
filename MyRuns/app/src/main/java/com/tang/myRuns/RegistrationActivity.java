package com.tang.myRuns;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class RegistrationActivity extends AppCompatActivity {

    // Event Constants
    private static final int CAMERA_TAKE_PICTURE = 100;
    private static final int REQUEST_GALLERY = 101;

    // Variables used across methods
    private Uri imgUri = null;
    private String imgPath = "";

    // UI References
    private ImageView profile;
    private SharedPreferences prefs;
    private EditText nameField;
    private EditText passwordField;
    private RadioGroup gender;
    private EditText emailField;
    private EditText phoneField;
    private EditText majorField;
    private EditText classField;
    private Button registrationButton;

    //State Variables
    private int isCropping = 0;
    private boolean passwordChanged = true;
    private boolean isRegistering;

    //Firebase
    private FirebaseAuth mAuth;
    private StorageReference mStorageRef;
    private FirebaseDatabase db;
    private DatabaseReference myRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db  = FirebaseDatabase.getInstance();
        myRef = db.getReference();
        mAuth = FirebaseAuth.getInstance();
        mStorageRef = FirebaseStorage.getInstance().getReference();
        setContentView(R.layout.activity_register);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        profile = findViewById(R.id.profile_pic);
        Button takeProfilePicButton = findViewById(R.id.camera_button);
        nameField = findViewById(R.id.regEditName);
        passwordField = findViewById(R.id.regEditPassword);
        passwordField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                passwordChanged = true;
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        gender = findViewById(R.id.radioGender);
        emailField = findViewById(R.id.regEditEmail);
        phoneField = findViewById(R.id.regEditPhone);
        majorField = findViewById(R.id.regEditMajor);
        classField = findViewById(R.id.regEditDartmouthClass);

        takeProfilePicButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPictureDialog();
            }
        });
        requestPerms();

        registrationButton = findViewById(R.id.registration_button);
        registrationButton.setText(R.string.registration_button);
        if(savedInstanceState != null){
            loadInstanceState(savedInstanceState);
        }
        registrationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptRegistration();
            }
        });
        Intent originalIntent = getIntent();
        String callingActivity = originalIntent.getStringExtra("SENDING_ACTIVITY_ID");
        if(!callingActivity.equals("SignInActivity")){
            loadFieldsFromPrefs();
            isRegistering = false;
        } else{
            isRegistering = true;
        }

    }

    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putInt("isCropping", isCropping);
        if(imgUri != null) {
            outState.putString("imgUri", imgUri.toString());
        }
        outState.putString("imgPath",imgPath);
    }

    public void loadInstanceState(Bundle savedInstanceState){
        isCropping = (int)savedInstanceState.get("isCropping");
        if(isCropping == 1){
            imgUri = Uri.parse((String)savedInstanceState.get("imgUri"));
            issueCropIntent(imgUri);
        }
        imgPath = savedInstanceState.getString("imgPath",null);
        if(!TextUtils.isEmpty(imgPath) && imgPath != null){
            updateProfilePic(profile, imgPath);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode == CAMERA_TAKE_PICTURE){
            if(resultCode == RESULT_OK){
                isCropping = 1;
                issueCropIntent(imgUri);
            }
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE){
            CropImage.ActivityResult r = CropImage.getActivityResult(data);
            if(resultCode == RESULT_OK){
                isCropping = 0;
                imgUri = r.getUri();
                try {
                    imgPath = savePic(MediaStore.Images.Media.getBitmap(this.getContentResolver(), imgUri));
                } catch (IOException e){
                    e.printStackTrace();
                }
                updateProfilePic(profile, imgPath);
            } else {
                isCropping = 0;
                Exception e = r.getError();
                e.printStackTrace();
            }
        } else if (requestCode == REQUEST_GALLERY){
            if(resultCode == RESULT_OK){
                if(data != null){
                    Uri selectedImage = data.getData();
                    issueCropIntent(selectedImage);
                }
            }
        }
    }

    private void submitForm(String email, final String phone, final int gender, final String name, final String dClass,
                            final String major, String pw, String path) {
        Uri file = Uri.fromFile(new File(imgPath));
        StorageReference pfpRef = mStorageRef.child("profiles/" + email + ".jpg");
        pfpRef.putFile(file);
        if (isRegistering) {
            mAuth.createUserWithEmailAndPassword(email, pw)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Toast.makeText(RegistrationActivity.this, "NICE MATE.",
                                        Toast.LENGTH_LONG).show();
                            } else {
                                // If sign in fails, display a message to the user.
                                Log.d("#firebase", "other stuff IG?");
                                Toast.makeText(RegistrationActivity.this, "Authentication failed.",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
            DatabaseReference userRef = myRef.child("accounts").child("" + email.hashCode()).child("metadata").child("user");
            Log.d("#firebase", "stuff ig");
            userRef.child("name").setValue(name);
            userRef.child("phone").setValue(phone);
            userRef.child("dClass").setValue(dClass);
            userRef.child("gender").setValue(gender);
            userRef.child("major").setValue(major);
            Log.d("#firebase", "Original" + userRef.toString());
        } else {
            FirebaseUser user = mAuth.getCurrentUser();
            user.updateEmail(email);
            if(passwordChanged) {
                user.updatePassword(pw);
            }
        }
    }
    private void loadFieldsFromPrefs(){
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        FirebaseUser user = mAuth.getCurrentUser();
        emailField.setText(user.getEmail());
        emailField.setFocusable(false);
        DatabaseReference userRef = myRef.child("accounts").child("" + user.getEmail().hashCode()).child("metadata").child("user");
        Log.d("#firebase", "later" + userRef.toString());
        userRef.child("dClass").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) { classField.setText(dataSnapshot.getValue(String.class));}
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
        userRef.child("phone").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) { phoneField.setText(dataSnapshot.getValue(String.class));}
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
        userRef.child("major").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String m = dataSnapshot.getValue(String.class);
                majorField.setText(m);
                Log.d("#firebase", m);}
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
        userRef.child("gender").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                RadioButton button = gender.findViewById(dataSnapshot.getValue(Integer.class));
                if(button != null){
                    button.setChecked(true);
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
        userRef.child("name").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                nameField.setText(dataSnapshot.getValue(String.class));
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
        StorageReference picRef = mStorageRef.child("profiles/" + user.getEmail() + ".jpg");
        File localFile;
        try {
            Log.d("#firebase", "nani");
            localFile = File.createTempFile("images", "jpg");
            picRef.getFile(localFile);
            Picasso.get().load(Uri.fromFile(localFile)).into(profile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        passwordChanged = false;
        registrationButton.setText(R.string.registration_button_save);
    }

    private void attemptRegistration(){
        boolean cancel = false;
        View focusView = null;
        if(TextUtils.isEmpty(imgPath)){
            focusView = profile;
            cancel = true;
            Toast.makeText(getApplicationContext(), "Profile picture required!", Toast.LENGTH_SHORT).show();
        }
        String name = nameField.getText().toString();
        if(TextUtils.isEmpty(name)){
            nameField.setError("This field is required");
            focusView = nameField;
            cancel = true;
        }
        String password = passwordField.getText().toString();
        if(!isPasswordValid(password) && (passwordChanged || isRegistering)){
            passwordField.setError("Please enter valid password");
            cancel = true;
            focusView = passwordField;
        }
        String email = emailField.getText().toString();
        if(TextUtils.isEmpty(email)){
            emailField.setError("This field is required");
            cancel = true;
            focusView = emailField;
        } else if (!isEmailValid(email)){
            emailField.setError("Enter valid email");
            cancel = true;
            focusView = emailField;
        }
        int genderId = gender.getCheckedRadioButtonId();
        RadioButton radioButton = gender.findViewById(genderId);
        if(radioButton == null) {
            RadioButton btn = findViewById(R.id.radioGenderF);
            btn.setError("This field is required");
            cancel = true;
            focusView = gender;
        }
        String phoneNum = phoneField.getText().toString();
        if(TextUtils.isEmpty(phoneNum)){
            phoneNum = "";
        }
        String major = majorField.getText().toString();
        if(TextUtils.isEmpty(major)){
            major = "";
        }
        String dClass = classField.getText().toString();
        if(TextUtils.isEmpty(dClass)){
            dClass = "";
        }
        if(cancel){
            focusView.requestFocus();
        } else {
            submitForm(email, phoneNum, genderId, name, dClass, major, password, imgPath);
            if(isRegistering) {
                Toast.makeText(getApplicationContext(), getString(R.string.registered_message), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.registered_message_save), Toast.LENGTH_LONG).show();
            }
            if(passwordChanged || isRegistering){
                Intent returnToSignUp = new Intent(RegistrationActivity.this, SignInActivity.class);
                returnToSignUp.putExtra("SENDING_ACTIVITY_ID", "RegistrationActivity");
                RegistrationActivity.this.startActivity(returnToSignUp);
            } else {
                finish();
            }
        }
    }

    /* ProfilePic-Related Methods */

    private void showPictureDialog(){
        CharSequence colors[] = new CharSequence[] {"Take from Camera", "Select from Gallery"};

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Profile Picture Picker");
        dialog.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(which == 0){
                    //Selected "Take from Camera"
                    takeProfilePic();
                } else {
                    selectGalleryPic();
                }
            }
        });
        dialog.show();
    }

    private void takeProfilePic() {
        if ((checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) ||
                (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            String errorMessage = "Error: lacking permissions!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
            return;
        }
        try {
            Intent picIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File file = new File(this.getExternalCacheDir(), String.valueOf(System.currentTimeMillis() + ".jpg"));
            imgUri = FileProvider.getUriForFile(this,
                    BuildConfig.APPLICATION_ID + ".provider", file);
            picIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            picIntent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);
            startActivityForResult(picIntent, CAMERA_TAKE_PICTURE);
        } catch (ActivityNotFoundException e) {
            String errorMessage = "Whoops - your device doesn't support capturing images!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private void selectGalleryPic(){
        if(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
            Intent picIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            picIntent.setType("image/*");
            startActivityForResult(Intent.createChooser(picIntent, "Select Image"), REQUEST_GALLERY);
        } else {
            String errorMessage = "Error: lacking permissions!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private void updateProfilePic(ImageView iv, String url){
        if(url != null) {
            iv.setImageURI(Uri.fromFile(new File(url)));
        }
    }

    private String savePic(Bitmap  bmp){
        File root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        String filePath = root.getAbsolutePath() + "/profile_pictures/" +
                System.currentTimeMillis() + ".jpg";
        File cachePath = new File(filePath);
        try {
            if (!cachePath.getParentFile().exists()){
                cachePath.getParentFile().mkdirs();
            }
            cachePath.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(cachePath);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.close();
            return filePath;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void issueCropIntent(Uri u){
        Intent intent = CropImage.activity(u).setAspectRatio(1,1).getIntent(getApplicationContext());
        startActivityForResult(intent, CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE);
    }



    /* Helper Methods */

    private void requestPerms() {
        /* we don't already have permission */
        if ((checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) ||
                (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
                    && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                return;
            }
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA}, 100);
        }
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

}