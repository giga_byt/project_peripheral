package com.tang.myRuns;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.LoaderManager;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tang.myRuns.services.ActivityDetectionService;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class MapActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<ExerciseEntry>, OnMapReadyCallback, ServiceConnection {

    private final static int LOADER_ID = 201;
    private final static int PERMISSION_REQUEST_CODE = 100;
    private final static int SECOND_INTERVAL = 1;
    public final static String DETECTED_ACTIVITY_BROADCAST = "detected_activity";

    private ServiceConnection connection = this;
    private final Messenger messenger = new Messenger(new IncomingMessageHandler());
    private Messenger serviceMessenger = null;

    private Button saveButton;
    private LoaderManager mLoader;
    private GoogleMap map;
    private TextView activityText, currSpeedText, speedText, climbText, calorieText, distanceText;

    private Marker currLoc;
    private Marker startLoc;
    private PolylineOptions rectOptions;
    private Polyline polyLine;

    private boolean isMetric = true;
    private boolean isRecord = false;
    private boolean isBound = false;
    private long recordId;

    private ExerciseEntry dataEntry;
    private String activity;
    private String input;
    private Calendar date;
    private int duration;
    private int durationInSeconds;
    private double distance;
    private double currSpeed;
    private double speed;
    private double climb;
    private double originalAlt;
    private int calorie;
    private ArrayList<Integer> timeList;
    private ArrayList<LatLng> locationList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            loadInstanceState(savedInstanceState);
        } else {
            if (getIntent().getStringExtra("SENDING_ACTIVITY_ID").equals("HistoryFragment")) {
                isRecord = true;
            }
            //Obtain MetaData
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            isMetric = preferences.getString("unit_switch", "Metric (Kilometers)").
                    equals("Metric (Kilometers)");
            if(!isRecord){
                input = getIntent().getStringExtra("INPUT_TYPE");
                if (input.equals("Automatic")) {
                    LocalBroadcastManager.getInstance(this).registerReceiver(detectActivityReceiver,
                            new IntentFilter(DETECTED_ACTIVITY_BROADCAST));
                    if(!ActivityDetectionService.isRunning()) {
                        startService(new Intent(this, ActivityDetectionService.class));
                    }
                } else {
                    activity = getIntent().getStringExtra("ACTIVITY_TYPE");
                }
            }
        }

        if (isRecord){
            recordId = getIntent().getLongExtra("ID", -1);
            mLoader = getSupportLoaderManager();
            mLoader.initLoader(LOADER_ID, null, this).forceLoad();
        } else {
            dataEntry = new ExerciseEntry();
            locationList = new ArrayList<>();
            date = Calendar.getInstance();
            durationInSeconds = 0;
        }
        //Initialize UI
        setContentView(R.layout.activity_map);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        activityText = findViewById(R.id.text_activity_type);
        currSpeedText = findViewById(R.id.text_cur_speed);
        speedText = findViewById(R.id.text_avg_speed);
        climbText = findViewById(R.id.text_climb);
        calorieText = findViewById(R.id.text_calories_burnt);
        distanceText = findViewById(R.id.text_distance);
        locationList = new ArrayList<>();
        if (!isRecord) {
            saveButton = findViewById(R.id.save_button);
            saveButton.setText(R.string.save_button);
            saveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    saveToDatabase();
                }
            });
        } else {
            saveButton = findViewById(R.id.save_button);
            saveButton.setText(R.string.button_delete);
            saveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DeleteDatabaseEntryTask run = new DeleteDatabaseEntryTask(getApplicationContext());
                    run.execute(recordId);
                    finish();
                }
            });
        }

        Intent tracker = new Intent(this, TrackerService.class);
        startService(tracker);

        //Launch Map
        SupportMapFragment mapFragment = (SupportMapFragment)
                getSupportFragmentManager().findFragmentById(R.id.map_activity_map);
        mapFragment.getMapAsync(this);
    }

    protected void loadInstanceState(Bundle savedInstanceState) {
        activity = savedInstanceState.getString("activity");
        input = savedInstanceState.getString("input");
        isRecord = savedInstanceState.getBoolean("isRecord");
        isMetric = savedInstanceState.getBoolean("isMetric");
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState){
        loadInstanceState(savedInstanceState);
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState){
        outState.putBoolean("isRecord", isRecord);
        outState.putBoolean("isMetric", isMetric);
        outState.putString("activity", activity);
        outState.putString("input", input);
        super.onSaveInstanceState(outState);
    }

    protected void onPause(){
        super.onPause();
        if(detectActivityReceiver != null){
            stopService(new Intent(this, ActivityDetectionService.class));
            LocalBroadcastManager.getInstance(this).unregisterReceiver(detectActivityReceiver);
        }
    }

    protected void onResume(){
        super.onResume();
    }

    private void saveToDatabase() {
        dataEntry.setmLocationList(locationList);
        dataEntry.setmActivityType(StartFragment.ACTIVITY_TO_ID_MAP.get(activity));
        dataEntry.setmInputType(StartFragment.INPUT_TO_ID_MAP.get(input));
        dataEntry.setmDateTime(date);
        dataEntry.setmDuration(duration);
        dataEntry.setmDistance(distance/1000);
        dataEntry.setmAvgSpeed(speed);
        dataEntry.setmClimb(climb);
        InsertDatabaseEntryTask run = new InsertDatabaseEntryTask(getApplicationContext());
        run.execute(dataEntry);
        unbind();
        Intent intent = new Intent(MapActivity.this, TrackerService.class);
        stopService(intent);
        Toast toast = Toast.makeText(this, "Saved Activity Details!", Toast.LENGTH_SHORT);
        toast.show();
        finish();
    }

    protected void onStop(){
        super.onStop();
    }

    protected void onDestroy(){
        super.onDestroy();
        unbind();
        Intent intent = new Intent(MapActivity.this, TrackerService.class);
        stopService(intent);
    }
    /*
       ----------------------------------------------- Loader Functions ----------------------------
     */

    @NonNull
    @Override
    public Loader<ExerciseEntry> onCreateLoader(int id, @Nullable Bundle args) {
        if(id == LOADER_ID) {
            return new SingleExerciseEntryLoader(getApplicationContext(), recordId);
        }
        return null;
    }

    @Override
    public void onLoadFinished(@NonNull Loader<ExerciseEntry> loader, ExerciseEntry data) {
        if(loader.getId() == LOADER_ID) {
            dataEntry = data;
            locationList = dataEntry.getmLocationList();
            if(map != null){
                initializeMapFromRecord();
            }
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<ExerciseEntry> loader) {
    }

    /*
       --------------------------------- Service-Related Functions ---------------------------------
     */

    private void bind(){
        if(TrackerService.isRunning()){
            bindToTracker();
        }
    }

    private void bindToTracker(){
        Intent service = new Intent(this, TrackerService.class);
        bindService(service, connection, Context.BIND_AUTO_CREATE);
        isBound = true;

    }

    private void unbind(){
        if(isBound){
            if(serviceMessenger != null){
                try{
                    Message msg = Message.obtain(null, TrackerService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = messenger;
                    serviceMessenger.send(msg);
                } catch (RemoteException e){}
            }

            unbindService(connection);
            isBound = false;
        }
    }

    public void onServiceConnected(ComponentName name, IBinder service){
        Log.d("#Binder", "EverConnected?");
        serviceMessenger = new Messenger(service);
        try{
            Message msg = Message.obtain(null, TrackerService.MSG_REGISTER_CLIENT);
            msg.replyTo = messenger;
            serviceMessenger.send(msg);
            msg = Message.obtain(null, TrackerService.MSG_INPUT_ACTIVITY_TYPE);
            Bundle bundle = new Bundle();
            bundle.putString("input", input);
            bundle.putString("activity", activity);
            msg.setData(bundle);
            msg.replyTo = messenger;
            serviceMessenger.send(msg);
        } catch (RemoteException e){}
    }

    public void onServiceDisconnected(ComponentName name){
        serviceMessenger = null;
    }

    private void sendActivityToService() {
        if (isBound) {
            if (serviceMessenger != null) {
                try {
                    Message msg = Message.obtain(null, TrackerService.MSG_INPUT_ACTIVITY_TYPE);// http://developer.android.com/intl/es/reference/android/os/Message.html#obtain()
                    Bundle bundle = new Bundle();
                    bundle.putString("input", input);
                    bundle.putString("activity", activity);
                    msg.setData(bundle);
                    msg.replyTo = messenger;
                    // we use the server messenger to send msg to the server
                    serviceMessenger.send(msg);
                } catch (RemoteException e) {
                }
            }
        }
    }
    private class IncomingMessageHandler extends Handler {
        public void handleMessage(Message msg){
            Log.d("#Map", "received?");
            Gson gson = new Gson();
            Type listType = new TypeToken<ArrayList<LatLng>>(){}.getType();
            locationList = gson.fromJson(msg.getData().getString("locationList_JSON"), listType);
            listType = new TypeToken<ArrayList<Integer>>(){}.getType();
            timeList = gson.fromJson(msg.getData().getString("timeList_JSON"), listType);
            activity = msg.getData().getString("frequentActivity");
            double altitude = msg.getData().getDouble("altitude");
            updateLocation(altitude);
        }
    }

    /*
       --------------- This section contains all map and map-related functions ---------------------
     */
    private LatLng locToLatLng(Location l){
        return new LatLng(l.getLatitude(), l.getLongitude());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        requestPerms();
        if(requestPerms()){
            if(isRecord) {
                initializeMapFromRecord();
            } else {
                bind();
            }
        }
    }

    private void initializeMapFromRecord(){
        if(dataEntry == null){
            return;
        } else {
            //Fill out text fields with pertinent info
            activity = StartFragment.ID_TO_ACTIVITY_MAP.get(dataEntry.getmActivityType());
            activityText.setText("Activity: " + activity);
            currSpeed = 0;
            if(locationList.size() != 0) {
                LatLng lastLoc = locationList.get(locationList.size()-1);
                LatLng secLastLoc = locationList.get(locationList.size()-2);
                double distanceTraveled = 0;
                float[] results = new float[1];
                Location.distanceBetween(lastLoc.latitude, lastLoc.longitude,
                        secLastLoc.longitude, secLastLoc.longitude, results);
                distanceTraveled = (double) results[0];
                currSpeed = distanceTraveled / SECOND_INTERVAL;
            }
            speed = dataEntry.getmAvgSpeed();
            climb = dataEntry.getmClimb();
            distance = dataEntry.getmDistance() * 1000;
            calorie = dataEntry.getmCalorie();
            calorieText.setText("Calories: " + calorie + "cal");
            DecimalFormat df = new DecimalFormat("#.##");
            if(isMetric) {
                currSpeedText.setText("Speed: " + df.format(currSpeed) + "m/s");
                speedText.setText("Avg Speed: " + df.format(speed) + "m/s");
                climbText.setText("Climb: " + df.format(climb) + "m");
                distanceText.setText("Distance: " + df.format(distance) + "m");
            }  else {
                double imperialSpeed = currSpeed * 3.28;
                currSpeedText.setText("Speed: " + df.format(imperialSpeed) + "ft/s");
                double imperialAvgSpeed = speed * 3.28;
                speedText.setText("Avg Speed: " + df.format(imperialAvgSpeed) + "ft/s");
                double imperialClimb = climb * 3.28;
                climbText.setText("Climb: " + df.format(imperialClimb) + "ft");
                double imperialDist = distance * 3.28;
                distanceText.setText("Distance: " + df.format(imperialDist) + "ft");
            }
            //Set Markers, Draw Path
            LatLng lastLoc = locationList.get(locationList.size()-1);
            LatLng firstLoc = locationList.get(0);
            currLoc = map.addMarker(new MarkerOptions().position(lastLoc).icon(
                    BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
            startLoc = map.addMarker(new MarkerOptions().position(firstLoc).icon(
                    BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));

            for(int i = 1; i < locationList.size(); i++){
                rectOptions = new PolylineOptions().add(locationList.get(i-1));
                rectOptions.add(locationList.get(i));
                rectOptions.color(Color.BLUE);
                polyLine = map.addPolyline(rectOptions);
            }
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(locationList.get(0), 15));
        }
    }

    private void updateLocation(double altitude) {
        if(locationList.size() > 0) {
            LatLng latlng = locationList.get(locationList.size()-1);
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng, 15));
            if(currLoc != null){
                currLoc.remove();
            }
            drawMap();
            double distanceTraveled = 0;
            double timeElapsed = 0;
            if(locationList.size() > 1) {
                LatLng prevLoc = locationList.get(locationList.size()-2);
                float[] results = new float[1];
                Location.distanceBetween(prevLoc.latitude, prevLoc.longitude,
                        latlng.latitude, latlng.longitude, results);
                distanceTraveled = (double) results[0];

                timeElapsed = timeList.get(timeList.size()-1) - timeList.get(timeList.size()-2);
            }
            currSpeed = distanceTraveled / timeElapsed;
            durationInSeconds = timeList.get(timeList.size()-1);
            duration = durationInSeconds/60;
            locationList.add(latlng);
            distance += distanceTraveled;
            speed = distance / ((double)durationInSeconds+1);
            climb += Math.max(altitude - originalAlt, 0);
            originalAlt = altitude;
            calorie = (int)(distance / 1000 * 59);
            calorieText.setText("Calorie: " + calorie + "cal");
            activityText.setText("Activity: " + activity);
            DecimalFormat df = new DecimalFormat("#.##");
            if(isMetric) {
                currSpeedText.setText("Speed: " + df.format(currSpeed) + "m/s");
                speedText.setText("Avg Speed: " + df.format(speed) + "m/s");
                climbText.setText("Climb: " + df.format(climb) + "m");
                distanceText.setText("Distance: " + df.format(distance) + "m");
            }  else {
                double imperialSpeed = currSpeed * 3.28;
                currSpeedText.setText("Speed: " + df.format(imperialSpeed) + "ft/s");
                double imperialAvgSpeed = speed * 3.28;
                speedText.setText("Avg Speed: " + df.format(imperialAvgSpeed) + "ft/s");
                double imperialClimb = climb * 3.28;
                climbText.setText("Climb: " + df.format(imperialClimb) + "ft");
                double imperialDist = distance * 3.28;
                distanceText.setText("Distance: " + df.format(imperialDist) + "ft");
            }
        }
    }

    private void drawMap(){
        LatLng lastLoc = locationList.get(locationList.size()-1);
        LatLng firstLoc = locationList.get(0);
        currLoc = map.addMarker(new MarkerOptions().position(lastLoc).icon(
                BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        startLoc = map.addMarker(new MarkerOptions().position(firstLoc).icon(
                BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));

        for(int i = 1; i < locationList.size(); i++){
            rectOptions = new PolylineOptions().add(locationList.get(i-1));
            rectOptions.add(locationList.get(i));
            rectOptions.color(Color.BLUE);
            polyLine = map.addPolyline(rectOptions);
        }
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(locationList.get(locationList.size() -1), 15));
    }

    /*
        Permission Check
     */
    public boolean requestPerms() {
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        }
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }
    BroadcastReceiver detectActivityReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Log.d(TAG, "onReceive()");
            if (intent.getAction().equals(DETECTED_ACTIVITY_BROADCAST)) {
                int type = intent.getIntExtra("type", -1);
                int confidence = intent.getIntExtra("confidence", 0);
                handleUserActivity(type, confidence);
            }
        }
    };
    private void handleUserActivity(int type, int confidence) {
        activity = "Other";
        switch (type) {
            case DetectedActivity.IN_VEHICLE: {
                activity = "Other";
                break;
            }
            case DetectedActivity.ON_BICYCLE: {
                activity = "Cycling";
                break;
            }
            case DetectedActivity.ON_FOOT: {
                activity = "Walking";
                break;
            }
            case DetectedActivity.RUNNING: {
                activity = "Running";
                break;
            }
            case DetectedActivity.STILL: {
                activity = "Standing";
                break;
            }
            case DetectedActivity.TILTING: {
                activity = "Other";
                break;
            }
            case DetectedActivity.WALKING: {
                activity = "Walking";
                break;
            }
            case DetectedActivity.UNKNOWN: {
                activity = "Other";
                break;
            }
        }
        if(confidence > 70){
            sendActivityToService();
        }
    }
}

class DummyListener implements LocationListener{

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}