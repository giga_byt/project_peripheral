package com.tang.myRuns;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by Tang on 4/23/2018.
 */

public class ExerciseEntryDbHelper extends SQLiteOpenHelper {

    public static final String TABLE_ENTRIES = "entries";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_INPUT = "input_type";
    public static final String COLUMN_ACTIVITY = "activity_type";
    public static final String COLUMN_DATE_TIME = "date_time";
    public static final String COLUMN_DURATION = "duration";
    public static final String COLUMN_DISTANCE = "distance";
    public static final String COLUMN_PACE = "avg_pace";
    public static final String COLUMN_SPEED = "avg_speed";
    public static final String COLUMN_CALORIES = "calories";
    public static final String COLUMN_CLIMB = "climb";
    public static final String COLUMN_HEART_RATE = "heartrate";
    public static final String COLUMN_COMMENT = "comment";
    public static final String COLUMN_PRIVACY = "privacy";
    public static final String COLUMN_GPS = "gps_data";

    private static final String DATABASE_NAME = "exercises.db";
    private static final int DATABASE_VERSION = 1;

    private FirebaseDatabase firebaseDatabase;
    private FirebaseAuth mAuth;
    private DatabaseReference myRef;

    private static final String DATABASE_CREATE = "create table if not exists " + TABLE_ENTRIES + "( "
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_INPUT + " integer not null, "
            + COLUMN_ACTIVITY + " integer not null, "
            + COLUMN_DATE_TIME + " text not null, "
            + COLUMN_DURATION + " integer not null, "
            + COLUMN_DISTANCE + " float, "
            + COLUMN_PACE + " float, "
            + COLUMN_SPEED + " float, "
            + COLUMN_CALORIES + " integer, "
            + COLUMN_CLIMB + " float, "
            + COLUMN_HEART_RATE + " integer, "
            + COLUMN_COMMENT + " text, "
            + COLUMN_PRIVACY + " integer, "
            + COLUMN_GPS + " text );";

    private SQLiteDatabase database;

    public ExerciseEntryDbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL(DATABASE_CREATE);

    }

    public void openDb() {
        database = this.getWritableDatabase();
    }

    public void closeDb() {
        this.close();
    }

    // Insert a item given each column value
    public ExerciseEntry insertEntry(ExerciseEntry entry) {
        boolean deleteOld = false;
        if(entry.getId() != null){
            if(CheckIsDataAlreadyInDBorNot(entry.getId())){
                Log.d("#firebase2", "" + entry.getId());
                return fetchEntryByIndex(entry.getId());
            } else {
                deleteOld = true;
            }
        }
        Gson gson = new Gson();
        ContentValues values = new ContentValues();
        values.put(COLUMN_INPUT, entry.getmInputType());
        values.put(COLUMN_ACTIVITY, entry.getmActivityType());
        values.put(COLUMN_DATE_TIME, entry.getmDateTime());
        values.put(COLUMN_DURATION, entry.getmDuration());
        values.put(COLUMN_DISTANCE, entry.getmDistance());
        values.put(COLUMN_PACE, entry.getmAvgPace());
        values.put(COLUMN_SPEED, entry.getmAvgSpeed());
        values.put(COLUMN_CALORIES, entry.getmCalorie());
        values.put(COLUMN_CLIMB, entry.getmClimb());
        values.put(COLUMN_HEART_RATE, entry.getmHeartRate());
        values.put(COLUMN_COMMENT, entry.getmComment());
        values.put(COLUMN_GPS, gson.toJson(entry.getmLocationList()));
        values.put(COLUMN_PRIVACY, entry.hasBeenShared());

        long insertId = database.insert(TABLE_ENTRIES, null, values);
        Cursor cursor = database.query(TABLE_ENTRIES, null, COLUMN_ID + " = " + insertId,
                null, null, null, null);
        cursor.moveToFirst();
        ExerciseEntry tentry = cursorToEntry(cursor);
        cursor.close();
        firebaseDatabase = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        myRef = firebaseDatabase.getReference().child("data").child(user.getUid());
        if(deleteOld){
            myRef.child("" + entry.getId()).setValue(null);
        }
        myRef.child("" + tentry.getId()).setValue(gson.toJson(tentry));
        return tentry;
    }

    // Remove an entry by giving its index
    public void removeEntry(long rowIndex) {
        Log.d("#db", "deleted " + rowIndex);
        ExerciseEntry tentry = fetchEntryByIndex(rowIndex);
        firebaseDatabase = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        myRef = firebaseDatabase.getReference().child("data").child(user.getUid());
        myRef.child("" + tentry.getId()).setValue(null);
        database.delete(TABLE_ENTRIES,COLUMN_ID + " = " + rowIndex, null);
    }

    // Query a specific entry by its index.
    public ExerciseEntry fetchEntryByIndex(long rowId) {
        Cursor cursor = database.query(TABLE_ENTRIES, null, COLUMN_ID + " = " + rowId, null,
                null, null, null);
        cursor.moveToFirst();
        return cursorToEntry(cursor);
    }

    public boolean CheckIsDataAlreadyInDBorNot(long rowId) {
        try {
            fetchEntryByIndex(rowId);
        } catch (CursorIndexOutOfBoundsException e){
            Log.d("#firebase2", "It isn't.");
            return false;
        }
        Log.d("#firebase2", "It is.");
        return true;
    }

    // Query the entire table, return all rows
    public ArrayList<ExerciseEntry> fetchEntries() {
        ArrayList<ExerciseEntry> entryList = new ArrayList<>();

        Cursor cursor = database.query(TABLE_ENTRIES, null, null,
                null, null, null, null);

        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            ExerciseEntry entry = cursorToEntry(cursor);
            entryList.add(entry);
            cursor.moveToNext();
        }

        cursor.close();
        return entryList;
    }

    private ExerciseEntry cursorToEntry(Cursor cursor){
        ExerciseEntry entry = new ExerciseEntry();
        entry.setId(cursor.getLong(0));
        entry.setmComment(cursor.getString(11));
        entry.setmInputType(cursor.getInt(1));
        entry.setmActivityType(cursor.getInt(2));
        entry.setmDateTime(cursor.getString(3));
        entry.setmDuration(cursor.getInt(4));
        entry.setmDistance(cursor.getDouble(5));
        entry.setmAvgPace(cursor.getLong(6));
        entry.setmAvgSpeed(cursor.getLong(7));
        entry.setmCalorie(cursor.getInt(8));
        entry.setmClimb(cursor.getLong(9));
        entry.setmHeartRate(cursor.getInt(10));
        entry.setmLocationList(cursor.getString(13));
        entry.setShared(cursor.getInt(12));
        return entry;
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("DROP TABLE IF EXISTS" + " ENTRIES");
    }


}
