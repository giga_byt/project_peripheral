# MyRuns 2

MyRuns2 was the project in which I continued to build the UI from MyRuns1. Introduced were
the brunt of MainActivity, an options bar which led to Settings and Profile, and a ManualActivity page.

# Installation

No special requirements are necessary; all libraries are already implemented in the build.gradle file.
The application simply needs to be built into an .apk file, and it'll be ready to install and use.

# Testing

There are no tests here, as it requires an emulator and physical handling to run for the edge cases.

# Built With

Basic Android, of course.

Also useful was the Android Image Cropper, an external cropping library, thanks to Arthur Teplitzki.