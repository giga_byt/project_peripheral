package com.example.tang.projectperipheral;

import java.util.UUID;

/**
 * Created by billt on 5/24/2018.
 */

public class Constant {
    public static final UUID MY_UUID = UUID.fromString("4c4ca170-5fca-11e8-9c2d-fa7ae01bbebc");
    public static final String appName = "project-peripheral";
}
